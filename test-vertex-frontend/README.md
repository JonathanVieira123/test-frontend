# Teste Vertex: Front-End
Desenvolver uma aplicação HTML5

- Essa aplicação foi desenvolvida utilizando React.js

# Para executa-lá siga as instruções: 

- Faça o Clone desse projeto 
- Crie um arquivo .env dentro da pasta do projeto (test-vertex-frontend) 
- Defina sua {API_KEY} como  REACT_APP_API_KEY=SUAAPIKEYAQUI 
- Via console utilize o comando "npm install" 
- Após isso, utilize o comando "npm start" para startar a aplicação

# Algumas Observações 

- A única validação do campo "Pesquisar" é feita em caso dele não ser preenchido.
- Para auxilio do desenvolvimento foram adicionadas as bibliotecas: Material-ui, Material-ui/icons, ReactPlayer, Axios e React-router-dom.


