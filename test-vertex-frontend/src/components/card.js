import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Card,
    CardActionArea,
    CardActions,
    CardMedia,
    CardContent,
    Typography
} from '@material-ui/core';
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
        marginTop: '0.4em',
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    details: { 
        color: '#1976D2', 
        textDecoration: 'none', 
        textTransform: 'uppercase', 
        fontWeight: '800', 
        marginLeft: '0.5em'
    }
});

export default function ImgMediaCard(props) {
    const classes = useStyles();

    const { videoId, title, description, img } = props

    return (
        <Card className={classes.root}>
            <CardActionArea>
                <CardMedia
                    component="img"
                    alt={title}
                    height="140"
                    image={img}
                    title={title}
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {title}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {description}
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                <Link className={classes.details} to={{
                    pathname: '/detalhes',
                    videoId: videoId,
                    title: title,
                    description: description
                }}>
                    Detalhes
            </Link>
            </CardActions>
        </Card>
    );
}