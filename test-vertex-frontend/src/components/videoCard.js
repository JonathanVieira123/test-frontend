import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Card,
    CardActionArea,
    CardActions,
    CardContent,
    Typography
} from '@material-ui/core';
import ReactPlayer from 'react-player';
import { 
    ThumbUpAlt,
    ThumbDown,
    Visibility,
} from '@material-ui/icons';
import './videoCard.css' 

const useStyles = makeStyles({
    root: {
        maxWidth: '640px',
        marginTop: '2em',
        marginLeft: 'auto',
        marginRight: 'auto',
    },
});

export default function VideoMediaCard(props) {
    const classes = useStyles();

    const { url, title, description, likeCount, dislikeCount, viewCount } = props

    return (
        <Card className={classes.root}>
            <CardActionArea>

                <ReactPlayer className="reactPlayer" url={url} playing />

                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {title}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {description}
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                {/* icons like, dislike e views */}
                <ThumbUpAlt /> {parseInt(likeCount)} <br></br>
                <ThumbDown />  {parseInt(dislikeCount)} <br></br>
                <Visibility /> {parseInt(viewCount)}
            </CardActions>
        </Card>
    );
}