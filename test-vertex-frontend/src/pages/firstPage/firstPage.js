import React, { useState, useEffect } from 'react';
import {
  makeStyles,
  TextField,
  Button,
} from '@material-ui/core';
import api from '../../services/api';
import ImgMediaCard from '../../components/card';
import './style.css';
import Loading from '../../components/loading';

const useStyles = makeStyles(theme => ({
  root: {
    '& .MuiInputBase-input': {
      color: 'white'
    },
    '& .Mui-error': {
      color: 'red'
    },
    '& label.Mui-focused': {
      color: 'white'
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: 'white'
    },
    '& .MuiInput-underline:before': {
      borderBottomColor: 'white'
    },
    '& label.MuiInputLabel-formControl': {
      color: 'white'
    },
    '& .MuiInput-underline:hover:after': {
      borderBottom: '1px solid white'
    },
    '& .MuiInput-underline:hover:before': {
      borderBottom: '2px solid white'
    },
    '& .MuiButton-contained.Mui-disabled': {
      color: 'gray',
    }
  },

  form: {
    margin: '90px auto 0',
    maxWidth: '450px',
    width: '100 %',
    display: 'table',
    flexDirection: 'column',
    alignItems: 'center',
    position: 'relative',
    marginBottom: '1em',
  },

  sendButton: {
    background: 'Transparent',
    border: '1px solid white',
    boxSizing: 'border-box',
    borderRadius: 8,
    color: 'white',
    marginTop: '1.2em',
    marginLeft: '3px',
  },

  paper: {
    display: 'flex',
    width: '50%',
    marginTop: '0.4em',
    marginLeft: 'auto',
    marginRight: 'auto',
    backgroundColor: 'initial',
    border: 'groove'

  },

  text: {
    marginLeft: '0.3em',
    marginRight: '0.3em%',
    marginTop: '0.1em',
    marginBottom: '0.1em',
    color: 'white'
  },

  pagination: {
    margin: '1px auto 0',
    display: 'table',
    flexDirection: 'column',
    alignItems: 'center',
    position: 'relative',
    marginBottom: '1em',
  },

  link: {
    color: 'white',
    fontStyle: 'italic',
    fontWeight: 'bolder',
  }
}))

export default function FirstPage(props) {
  const classes = useStyles();

  const [search, setSearch] = useState('')
  const [spots, setSpots] = useState([])
  const [newPageToken, setNewPageToken] = useState('')
  const [prevPageToken, setPrevPageToken] = useState('')
  const [flag, setFlag] = useState(false)
  const [slideInUp, setSlideInUp] = useState(false)
  const [error, setError] = useState(false)
  const [loading, setLoading] = useState(false)

  const ref = React.createRef()

  useEffect(() => {
    if (props.location.state !== undefined) {
      setSpots(props.location.state.spots)
      setNewPageToken(props.location.state.newPageToken)
      setPrevPageToken(props.location.state.prevPageToken)
      setFlag(props.location.state.flag)
      setSlideInUp(props.location.state.slideInUp)
    }
  })

  const handleSearch = async e => {
    e.preventDefault();
    try {
      if (search.length === 0) {
        setError(true)
      } else {
        setError(false)
        setLoading(true)
        const response = await api.get('/search?part=id,snippet&maxResults=5&order=viewCount&type=video&q=' + search + '&key=' + process.env.REACT_APP_API_KEY)
        setLoading(false)
        setSpots(response.data.items)
        setNewPageToken(response.data.nextPageToken)
        setFlag(true)
        setSlideInUp(true)
        props.history.push({
          pathname: '/results',
          state: {
            spots: response.data.items,
            newPageToken: response.data.nextPageToken,
            prevPageToken: response.data.prevPageToken,
            flag: true,
            slideInUp: true,
          }
        })
      }
    } catch (error) {
      console.log(error)

    }
  }

  const handlePage = async (e, pageToken) => {
    e.preventDefault();
    try {
      ref.current.scrollIntoView({
        behavior: 'smooth',
        block:'start',
        inline: 'nearest'
      })
      setLoading(true)
      const response = await api.get('/search?part=id,snippet&maxResults=5&order=viewCount&type=video&pageToken=' + pageToken + '&q=' + search + '&key=' + process.env.REACT_APP_API_KEY)
      setLoading(false)
      setSpots(response.data.items)
      setNewPageToken(response.data.nextPageToken)
      setPrevPageToken(response.data.prevPageToken)
      props.history.push({
        pathname: '/results',
        state: {
          spots: response.data.items,
          newPageToken: response.data.nextPageToken,
          prevPageToken: response.data.prevPageToken,
          flag: true,
          slideInUp: true,
        }
      })
    } catch (error) {

    }
  }


  return (

    <div className={classes.root}  ref={ref}>
      <div className={slideInUp ? 'slideInUp' : ''} >
        <form className={classes.form}>
          <TextField
            error={error}
            id="search"
            name="search"
            label="Pesquisar"
            placeholder="Pesquisar"
            helperText={error ? '* Por favor preencha esse campo' : ''}
            value={search}
            onChange={event => setSearch(event.target.value)}
          />

          <Button className={classes.sendButton}
            type="submit"
            size="small"
            variant="contained"
            onClick={handleSearch}
          >
            Buscar
        </Button>
        </form >
        {loading && (<Loading />)}
      </div>
      {
        spots.map(spot => (
          <ImgMediaCard key={spot.id.videoId}
            videoId={spot.id.videoId}
            title={spot.snippet.title}
            description={spot.snippet.description}
            img={spot.snippet.thumbnails.high.url}
          />
        ))
      }

      <div className={classes.pagination}>
        {flag && (<Button className={classes.sendButton}
          type="submit"
          size="small"
          variant="contained"
          disabled={prevPageToken === undefined || prevPageToken === ''}
          onClick={event => handlePage(event, prevPageToken)}
        >
          Página Anterior
        </Button>)}

        {flag && (<Button className={classes.sendButton}
          type="submit"
          size="small"
          variant="contained"
          disabled={newPageToken === undefined || prevPageToken === ''}
          onClick={event => handlePage(event, newPageToken)}
        >
          Próxima Página
        </Button>)}

      </div>
    </div >
  )
} 