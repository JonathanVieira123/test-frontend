import React, { useState, useEffect } from 'react';
import api from '../../services/api';
import VideoMediaCard from '../../components/videoCard';
import { Button, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    center: {
        margin: '10px auto 0',
        display: 'table',
        flexDirection: 'column',
        alignItems: 'center',
        position: 'relative',
        marginBottom: '1em',
    },
    sendButton: {
        background: 'Transparent',
        border: '1px solid white',
        boxSizing: 'border-box',
        borderRadius: 8,
        color: 'white',
        marginTop: '1.2em',
        marginLeft: '3px',
    },

}))

export default function SecondPage(props) {

    const classes = useStyles();

    const [videoId, setVideoId] = useState('')
    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')

    const [likeCount, setLikeCount] = useState('')
    const [dislikeCount, setDislikeCount] = useState('')
    const [viewCount, setViewCount] = useState('')

    useEffect(() => {
        setVideoId(props.location.videoId)
        setTitle(props.location.title)
        setDescription(props.location.description)
    }, [])

    useEffect(() => {
        getDetails()
    }, [videoId])

    const getDetails = async e => {
        try {
            if (videoId !== undefined) {
                const response = await api.get('/videos?id=' + props.location.videoId + '&part=snippet,statistics&key=' + process.env.REACT_APP_API_KEY)
                let statistics = response.data.items[0].statistics

                setLikeCount(statistics.likeCount)
                setDislikeCount(statistics.dislikeCount)
                setViewCount(statistics.viewCount)
            }
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <div>
            <VideoMediaCard key={videoId}
                url={"https://www.youtube.com/watch?v=" + videoId}
                title={title}
                description={description}
                likeCount={likeCount}
                dislikeCount={dislikeCount}
                viewCount={viewCount}
            />
            <div className={classes.center}>
                <Button
                    className={classes.sendButton}
                    type="submit"
                    size="small"
                    variant="contained"
                    onClick={() => props.history.goBack()}
                >
                    Voltar
        </Button>
            </div>


        </div>
    )
}