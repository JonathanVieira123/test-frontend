import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { withRouter } from 'react-router';
import FirstPage from './pages/firstPage/firstPage';
import SecondPage from './pages/secondPage/secondPage';

class Routes extends Component {
 
    render() {
        
        return (
            <Switch>
                <Route exact path="/" component={FirstPage} />
                <Route path="/results" component={FirstPage} />
                <Route path="/detalhes" component={SecondPage} />
                <Route path="*" component={() => <h1>Page not found</h1>} />
            </Switch>
        );
    }
}

export default withRouter(Routes);